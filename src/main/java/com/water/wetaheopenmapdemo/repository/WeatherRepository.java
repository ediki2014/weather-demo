package com.water.wetaheopenmapdemo.repository;

import com.water.wetaheopenmapdemo.model.CurrentWeather;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface WeatherRepository extends MongoRepository<CurrentWeather, String> {
    @Query(value = "{name:?0}")
    List<CurrentWeather> findByName(String name);
}
