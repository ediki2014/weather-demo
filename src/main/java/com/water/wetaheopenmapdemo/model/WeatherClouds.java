package com.water.wetaheopenmapdemo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherClouds {
    private int all;

    public WeatherClouds() {
        this.all = 0;
    }

    public WeatherClouds(int all) {
        this.all = all;
    }

    public int getAll() {
        return all;
    }

    public void setAll(int all) {
        this.all = all;
    }
}
