package com.water.wetaheopenmapdemo.model;

public interface WeatherForecast {
    public String format();
}
