package com.water.wetaheopenmapdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WetaheOpenMapDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(WetaheOpenMapDemoApplication.class, args);
	}
}
