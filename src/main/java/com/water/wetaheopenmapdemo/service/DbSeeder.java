package com.water.wetaheopenmapdemo.service;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.water.wetaheopenmapdemo.model.CurrentWeather;
import com.water.wetaheopenmapdemo.repository.WeatherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.Arrays;
import java.util.List;

@Component
public class DbSeeder implements CommandLineRunner {

    @Autowired
    private WeatherRepository weatherRepository;

    public DbSeeder(WeatherRepository weatherRepository) {
        this.weatherRepository = weatherRepository;
    }

    @Override
    public void run(String... strings) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

        URL url = new URL("http://api.openweathermap.org/data/2.5/weather?q=Mogilev&mode=json&units=metric&appid=c3a3c1e376fffdfb9d8ddc388c488756");
        CurrentWeather citySys1 = objectMapper.readValue(url, CurrentWeather.class);
        List<CurrentWeather> citySys = Arrays.asList(citySys1);
        this.weatherRepository.save(citySys);

    }


}
