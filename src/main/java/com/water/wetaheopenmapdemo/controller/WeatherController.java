package com.water.wetaheopenmapdemo.controller;



import com.fasterxml.jackson.databind.ObjectMapper;
import com.water.wetaheopenmapdemo.model.CurrentWeather;
import com.water.wetaheopenmapdemo.repository.WeatherRepository;

import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.*;
import java.util.Arrays;
import java.util.List;


@RestController
@RequestMapping("/city")
public class WeatherController {
    private WeatherRepository weatherRepository;

    public WeatherController(WeatherRepository weatherRepository) {
        this.weatherRepository = weatherRepository;
    }

    @GetMapping("/all")
    public List<CurrentWeather> getAll() {
        List<CurrentWeather> citySys = this.weatherRepository.findAll();
        return citySys;
    }


    @RequestMapping(value = "/{name}", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
    public CurrentWeather getNameCityWeather(@PathVariable String name) throws IOException {
        String apiKey = "c3a3c1e376fffdfb9d8ddc388c488756";//Removed my  API Key for posting.
        URL url = new URL("http://api.openweathermap.org/data/2.5/weather?q="+name+"&mode=json&units=metric&appid="+apiKey+"");
        ObjectMapper objectMapper = new ObjectMapper();
        CurrentWeather getCityWeath = objectMapper.readValue(url, CurrentWeather.class);
        List<CurrentWeather> citySys = Arrays.asList(getCityWeath);
        this.weatherRepository.save(citySys);
        return getCityWeath;
    }

}
